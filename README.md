> DEMO https://gitlab.com/huangju/mock-demo

> 参考 https://www.jianshu.com/p/ac23664982b2

## 运行
```
yarn
//运行express + mock
npm run mock
//运行react
npm start
```

## 集成mock
```
yarn add mockjs
```

```
//mock.js 随机模拟数据

var Mock = require("mockjs")
var express = require("express")
var router = express.Router();

router.use("/profile",function (req,res) {
    console.log(req.body);
    //调用mock方法模拟数据
    var data = Mock.mock({
            // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
            'list|1-10': [{
                // 属性 id 是一个自增数，起始值为 1，每次增 1
                'id|+1': 1
            }]
        }
    );
    return res.json(data);
})

module.exports = router;
```

## 跨域代理
```
yarn add http-proxy-middleware
```

新建src/setupProxy.js（名字固定）
```
const proxy = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(proxy('/api', { target: 'http://localhost:3001/' }))
}
```

